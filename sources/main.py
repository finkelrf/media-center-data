from db import InfluxDb
from systemInfo import CpuInfo, GpuInfo, MemInfo
import config
from threading import Thread
import time

def task(db, field, cb):
  while True:
    db.add(field, cb())
    time.sleep(2)

if __name__ == "__main__":
  gpu = GpuInfo()
  cpu = CpuInfo()
  mem = MemInfo()
  db = InfluxDb("telemetry", config)

  Thread(target=task, args=[db, "cpu_usage", cpu.usage]).start()
  Thread(target=task, args=[db, "cpu_frequency", cpu.frequency]).start()
  Thread(target=task, args=[db, "cpu_temp", cpu.temperature]).start()
  Thread(target=task, args=[db, "cpu_fan", cpu.fan]).start()

  Thread(target=task, args=[db, "gpu_cuda_clock", gpu.cuda_clock]).start()
  Thread(target=task, args=[db, "gpu_fan", gpu.fan_speed]).start()
  Thread(target=task, args=[db, "gpu_memory_clock", gpu.memory_clock]).start()
  Thread(target=task, args=[db, "gpu_memory_used", gpu.memory_used]).start()
  Thread(target=task, args=[db, "gpu_power_draw", gpu.power_draw]).start()
  Thread(target=task, args=[db, "gpu_shaders_clock", gpu.shaders_clock]).start()
  Thread(target=task, args=[db, "gpu_temp", gpu.temperature]).start()
  Thread(target=task, args=[db, "gpu_usage", gpu.usage]).start()

  Thread(target=task, args=[db, "mem_usage", mem.usage]).start()
  Thread(target=task, args=[db, "mem_used", mem.used]).start()
