import influxdb_client
import os
import time
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS

class InfluxDb():
  def __init__(self, point, config):
    self._config = config
    self._point = point
    self._client = influxdb_client.InfluxDBClient(
        url=config.url, token=config.token, org=config.org)
    self._write_api = self._client.write_api(write_options=SYNCHRONOUS)

  def add(self, field, value):
    point = Point(self._point).field(field, value)
    self._write_api.write(bucket=self._config.bucket, org=self._config.org, record=point)

  def query(self):
    query_api = self._client.query_api()

    query = """from(bucket: "media-center")
    |> range(start: -10m)
    |> filter(fn: (r) => r._measurement == "telemetry")"""
    tables = query_api.query(query, org=self._client.org)

    for table in tables:
      for record in table.records:
        print(record)


if __name__ == "__main__":
  import config
  db = InfluxDb("telemetry", config)
  db.add("test", 10.0)