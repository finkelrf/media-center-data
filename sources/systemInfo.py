import psutil
import re
import subprocess as sp

def output_to_list(x): return x.decode('ascii').split('\n')[:-1]

def dataHandlerPassAll(data): return data

def onlyNumbers(data):
    found = re.search("[0-9/.]*", data)
    try:
      return float(found.group()) if found != None else None
    except ValueError:
      return None

class GpuInfo():
  def _query(self, data, dataHandlerCb=dataHandlerPassAll):
    COMMAND = f"nvidia-smi --query-gpu={data} --format=csv"
    try:
        output = output_to_list(sp.check_output(COMMAND.split(), stderr=sp.STDOUT))[1:]
    except sp.CalledProcessError as e:
        raise RuntimeError("command '{}' return with error (code {}): {}".format(
            e.cmd, e.returncode, e.output))
    return dataHandlerCb(output[0]) if len(output) > 0 else None

  def usage(self):
    return self._query("utilization.gpu", onlyNumbers)

  def fan_speed(self):
    return self._query("fan.speed", onlyNumbers)

  def memory_used(self):
    return self._query("memory.used", onlyNumbers)

  def temperature(self):
    return self._query("temperature.gpu", onlyNumbers)

  def power_draw(self):
    return self._query("power.draw", onlyNumbers)

  def memory_clock(self):
    return self._query("clocks.current.memory", onlyNumbers)

  def shaders_clock(self):
    return self._query("clocks.current.graphics", onlyNumbers)

  def cuda_clock(self):
    return self._query("clocks.current.sm", onlyNumbers)


class CpuInfo():
  def usage(self):
    return psutil.cpu_percent()

  def frequency(self):
    return psutil.cpu_freq().current
  
  def temperature(self):
    temps = psutil.sensors_temperatures()
    return temps['coretemp'][0].current if 'coretemp' in temps.keys() else None
  
  def fan(self):
    fans = psutil.sensors_fans()
    return psutil.sensors_fans()['asus'][0].current if "asus" in fans.keys() else None

class MemInfo():
  def usage(self):
    return psutil.virtual_memory().percent
  
  def used(self):
    return psutil.virtual_memory().used



if __name__ == "__main__":
  c = CpuInfo()
  print(c.fan())
  print(c.frequency())
  print(c.temperature())
  
